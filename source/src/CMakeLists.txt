cmake_minimum_required(VERSION 2.8)

project(stm32f103_dual_cdc)

set(PP_DEFINES " -DSTM32 -DSTM32F1 -DSTM32F103C8Tx -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER")
#set(PP_DEFINES "${PP_DEFINES} ")

include_directories(
    ${libs_SOURCE_DIR}/CMSIS/device
    ${libs_SOURCE_DIR}/CMSIS/core
    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/inc
    ${libs_SOURCE_DIR}/STM32_USB-FS-Device_Driver/inc
    inc
)

# uncomment/ comment as required
file(GLOB STM_STD_PERIPH_LIB_SRC
    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/misc.c
#    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_adc.c
#    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_bkp.c
#    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_can.c
#    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_cec.c
#    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_crc.c
#    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_dac.c
#    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_dbgmcu.c
    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_dma.c
    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_exti.c
#    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_flash.c
#    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_fsmc.c
    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_gpio.c
#    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_i2c.c
#    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_iwdg.c
    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_pwr.c
    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_rcc.c
#    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_rtc.c
#    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_sdio.c
#    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_spi.c
    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_tim.c
    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_usart.c
#    ${libs_SOURCE_DIR}/STM32F10x_StdPeriph_Driver/src/stm32f10x_wwdg.c
)

file(GLOB STM32_USB_FS_DEVICE_SRC
    ${libs_SOURCE_DIR}/STM32_USB-FS-Device_Driver/src/usb_core.c
    ${libs_SOURCE_DIR}/STM32_USB-FS-Device_Driver/src/usb_init.c
    ${libs_SOURCE_DIR}/STM32_USB-FS-Device_Driver/src/usb_int.c
    ${libs_SOURCE_DIR}/STM32_USB-FS-Device_Driver/src/usb_mem.c
    ${libs_SOURCE_DIR}/STM32_USB-FS-Device_Driver/src/usb_regs.c
    ${libs_SOURCE_DIR}/STM32_USB-FS-Device_Driver/src/usb_sil.c
)


file(GLOB C_SOURCE
    hw_config.c
    main.c
    stm32f10x_it.c
    syscalls.c
    system_stm32f10x.c
    usb_desc.c
	usb_endp.c
	usb_istr.c
	usb_prop.c
	usb_pwr.c

    ${STM_STD_PERIPH_LIB_SRC}
    ${STM32_USB_FS_DEVICE_SRC}
)

set_source_files_properties(${C_SOURCE}
    PROPERTIES COMPILE_FLAGS ${PP_DEFINES}
)

add_executable(${PROJECT_NAME}.elf
    ${STARTUP_ASM_FILE}
    ${C_SOURCE}
)

target_link_libraries(${PROJECT_NAME}.elf)

add_custom_target ( ${PROJECT_NAME}.hex ALL
    DEPENDS ${PROJECT_NAME}.elf 
    COMMAND ${CMAKE_OBJCOPY} -O ihex ${PROJECT_NAME}.elf ${PROJECT_NAME}.hex
    COMMENT "Generating ${PROJECT_NAME}.hex"
)
add_custom_target ( ${PROJECT_NAME}.bin ALL
    DEPENDS ${PROJECT_NAME}.elf 
    COMMAND ${CMAKE_OBJCOPY} -O binary ${PROJECT_NAME}.elf ${PROJECT_NAME}.bin
    COMMENT "Generating ${PROJECT_NAME}.bin"
)

# debug target
add_custom_target ( debug-${PROJECT_NAME}
    DEPENDS ${PROJECT_NAME}.elf 
    COMMAND ${CMAKE_GDB} ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.elf
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    COMMENT "Starting gdb for ${PROJECT_NAME}"
)
