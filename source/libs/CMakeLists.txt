cmake_minimum_required(VERSION 2.8)

project(libs)

add_subdirectory(CMSIS)

# Standard peripheral driver for stm32f10x
add_subdirectory(STM32F10x_StdPeriph_Driver)

# USB libs for stm32f10x
add_subdirectory(STM32_USB-FS-Device_Driver)

# project start-up file
add_subdirectory(startup)

