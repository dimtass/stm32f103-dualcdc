Dual CDC for STM32F103
----

### Description

This project is forked from [here](https://github.com/x893/STM32F103-DualCDC) and is updated to the latest CMSIS (5.1.0) and STM32 USB library (4.1.0) and I've also made it to build cmake.

### Notes
This fork is for stm32f103c8tx. If you want to use any other stmf10x then you need to:
- add the correct linker script in _source/libs_ and then replace the _STM32F103C8Tx.ld_ in _source/CMakeLists.txt_ with the correct filename.
- do the same for the startup file and replace _startup_stm32f10x_md.s_ in _source/CMakeLists.txt_ with the correct filename from _source/libs/startup_

### How to compile and flash
You need cmake to build this project either on Windows or Linux. To setup the cmake properly follow the instructions from [here](https://github.com/dimtass/cmake_toolchains/blob/master/README.md). Then edit the cmake/TOOLCHAIN_arm_none_eabi_cortex_m3.cmake file and point TOOLCHAIN_DIR to the correct GCC path.
> e.g. on Windows
> set(TOOLCHAIN_DIR C:/opt/gcc-arm-none-eabi-4_9-2015q3-20150921-win32)
> or on Linux
> set(TOOLCHAIN_DIR /opt/gcc-arm-none-eabi-4_9-2015q3)

Then on Windows run ```build.cmd``` or on Linux run ```./build.sh``` and the .bin and .hex files should be created in the ```build-stm32/src``` folder. Also, a .cproject and .project files are created if you want to edit the source code.

To flash the HEX file in windows use st-link utility like this:
```"C:\Program Files (x86)\STMicroelectronics\STM32 ST-LINK Utility\ST-LINK Utility\ST-LINK_CLI.exe" -c SWD -p build-stm32\src\stm32f103_wifi_usb_psu.hex -Rst```

To flash the bin in Linux:
```st-flash --reset write build-stm32/src/stm32f103_wifi_usb_psu.bin 0x8000000```

### Usage
When the bin is flashed to the STM32 then connect the USB cable and two comm ports will be enumerated.